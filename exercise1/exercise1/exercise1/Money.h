#pragma once
#include<iostream>
class Money
{
private:
	int m_euros;
	int m_centimes;
public:
	Money();
	void print();
	Money(int _euro, int _centimes);
	void setEuros(int _euros);
	void setCentimes(int _centimes);
	int getEuros();
	int getCentimes();
	~Money();

	friend std::istream &operator>>(std::istream &inputStream, Money &_money);
	friend std::ostream &operator<<(std::ostream &outStream, Money _money);

	Money operator+(Money _money);
	Money operator-(Money _money);
	Money operator*(Money _money);
	Money operator/(Money _money);

	bool operator==(Money _money);
	bool operator!=(Money _money);
	bool operator<(Money _money);
	bool operator<=(Money _money);
	bool operator>(Money _money);
	bool operator>=(Money _money);
};

