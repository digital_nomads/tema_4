#include"Money.h"

using namespace std;

void main()
{
	try
	{
		Money m(2, 99);
		(m + m + m).print();
		std::cout << endl;

		Money m1(10, 88);
		Money m2(5, 22);
		Money m3(5, 22);
		(m1 / m2).print();
		std::cout << endl;
		//Money negativeMoney(-1, 0);
		//Money m0(0, 0);
		//(m1 / m0).print();
		cout << "Read from keyboard: ";
		Money m4;
		cin >> m4;
		cout << m4 << endl;

		(m1 * m2).print();
		cout << endl;

		cout << (m1 != m2) << endl;
		cout << (m2 == m3) << endl;
		cout << (m2 < m1) << endl;
		cout << (m2 >= m1) << endl;
		cin.get();
	}
	catch(exception e)
	{
		cout << e.what();
	}
	cin.get();
}