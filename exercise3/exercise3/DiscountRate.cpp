#include "DiscountRate.h"


DiscountRate::DiscountRate()
{
	m_serviceDiscountPremium = 0.2;
	m_serviceDiscountGold =0.15;
	m_serviceDiscountSilver = 0.1;
	m_productDiscountPremium = 0.1;
	m_productDiscountGold = 0.1;
	m_productDiscountSilver = 0.1;
}

DiscountRate::DiscountRate(const double _silverDiscount, const  double _goldDiscount,const  double _premiumDiscount)
{
	m_serviceDiscountPremium = _premiumDiscount;
	m_serviceDiscountGold = _goldDiscount;
	m_serviceDiscountSilver = _silverDiscount;
	m_productDiscountPremium = _premiumDiscount;
	m_productDiscountGold = _goldDiscount;
	m_productDiscountSilver = _silverDiscount;
}

void DiscountRate::setServiceDiscount(const Membership::MemberType _memberType, const  double _discountRate)
{
	switch (_memberType)
	{
	case(Membership::Premium): m_serviceDiscountPremium = _discountRate; break;
	case(Membership::Gold): m_serviceDiscountGold = _discountRate; break;
	case(Membership::Silver): m_serviceDiscountSilver = _discountRate; break;
	}
}

void DiscountRate::setProductDiscount(const Membership::MemberType _memberType,const double _discountRate)
{
	switch (_memberType)
	{
	case(Membership::Premium): m_productDiscountPremium = _discountRate; break;
	case(Membership::Gold): m_productDiscountGold = _discountRate; break;
	case(Membership::Silver): m_productDiscountSilver = _discountRate; break;
	}
}

double DiscountRate::getServiceDiscount(const Membership::MemberType _memberType)
{
	switch (_memberType)
	{
	case(Membership::Premium): return m_serviceDiscountPremium;
	case(Membership::Gold): return m_serviceDiscountGold;
	case(Membership::Silver): return m_serviceDiscountSilver;
	}
	return 0.0;
}

double DiscountRate::getProductDiscount(const Membership::MemberType _memberType)
{
	switch (_memberType)
	{
	case(Membership::Premium): return m_productDiscountPremium;
	case(Membership::Gold): return m_productDiscountGold;
	case(Membership::Silver): return m_productDiscountSilver;
	}
	return 0.0;
}


DiscountRate::~DiscountRate()
{
}
