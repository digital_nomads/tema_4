#include "Date.h"



Date::Date()	//Default constructor "should" initialize the date with current date, but an equivalent of datetime.now() in C++ is a pain.
{
	m_day = 1;
	m_month = 4;
	m_year = 2017;
}

Date::Date(const int _day, const int _month, const int _year)
{
	m_day = _day;
	m_month = _month;
	m_year = _year;
}

std::string Date::toString()
{
	return "" + m_day + '.' + m_month + '.' + m_year;
}

int Date::getDay()
{
	return m_day;
}

int Date::getMonth()
{
	return m_month;
}

int Date::getYear()
{
	return m_year;
}


Date::~Date()
{
}
