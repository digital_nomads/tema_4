#pragma once
#include<string>
class Date
{
private:
	int m_day;
	int m_month;
	int m_year;
public:
	Date();
	Date(const int _day, const int _month, const int _year);
	std::string toString();
	int getDay();
	int getMonth();
	int getYear();
	~Date();
};

