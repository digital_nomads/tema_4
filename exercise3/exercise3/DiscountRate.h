#pragma once
#include<string>
#include "memberTypeEnum.h"
class DiscountRate
{
private:
	double m_serviceDiscountPremium;
	double m_serviceDiscountGold;
	double m_serviceDiscountSilver;
	double m_productDiscountPremium;
	double m_productDiscountGold;
	double m_productDiscountSilver;
public:
	DiscountRate();
	DiscountRate(const double, const  double, const  double);
	void setServiceDiscount(const Membership::MemberType, const double);
	void setProductDiscount(const Membership::MemberType, const double);
	double getServiceDiscount(const Membership::MemberType);
	double getProductDiscount(const Membership::MemberType);
	~DiscountRate();
};

