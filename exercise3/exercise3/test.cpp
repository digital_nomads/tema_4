#include"BeautySaloon.h"
#include<iostream>
using namespace std;

void main()
{
	BeautySaloon uglyBeGone;
	Customer c1("Vlad", Membership::Premium);
	Customer c2("Mircea", Membership::Silver);
	uglyBeGone.addCustomer(c1);
	uglyBeGone.addCustomer(c2);
	uglyBeGone.addCustomer("Silvia");
	uglyBeGone.addCustomer("Miruna", Membership::Premium);

	uglyBeGone.addVisit(c1, 100, 100);
	uglyBeGone.addVisit(c2, 100, 0);

	cout << "Total income: " << uglyBeGone.getTotalIncome() << endl;
	cout << "Services income: " << uglyBeGone.getServicesIncome() << endl;
	cout << "Products income: " << uglyBeGone.getProductsIncome() << endl;


	cout << "\nCustomers:\n";
	uglyBeGone.printCustomers();

	cin.get();
}