#include "Customer.h"

Customer::Customer()
{
}

Customer::Customer(const std::string _name, const  Membership::MemberType _memberType)
{
	m_name = _name;
	m_member = true;
	m_memberType = _memberType;
}

Customer::Customer(const std::string _name)
{
	m_name = _name;
	m_member = false;
}

void Customer::setMemberType(const Membership::MemberType _memberType)
{
	m_memberType = _memberType;
}

bool Customer::isMember()
{
	return m_member;
}

Membership::MemberType Customer::getMemberType()
{
	return m_memberType;
}

std::string Customer::getName()
{
	return m_name;
}

std::string Customer::toString()
{
	std::string returnString = m_name;
	if (m_member)
	{
		returnString = returnString + " - ";
		switch (m_memberType)
		{
		case(Membership::Premium): returnString += "Premium"; break;
		case(Membership::Gold): returnString += "Gold"; break;
		case(Membership::Silver): returnString += "Silver"; break;
		}
	}
	else
		returnString = returnString + " - not member";
	return returnString;
}


Customer::~Customer()
{
}
