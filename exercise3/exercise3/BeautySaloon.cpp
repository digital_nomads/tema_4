#include "BeautySaloon.h"
#include<iostream>


BeautySaloon::BeautySaloon()
{
	m_totalServicesIncome = 0.0;
	m_totalProductsIncome = 0.0;
}

BeautySaloon::BeautySaloon(const std::string _saloonName)
{
	m_saloonName = _saloonName;
	m_totalServicesIncome = 0.0;
	m_totalProductsIncome = 0.0;
}

BeautySaloon::BeautySaloon(const std::string _saloonName, const DiscountRate _discountRate)
{
	m_saloonName = _saloonName;
	m_discountRates = _discountRate;
	m_totalServicesIncome = 0.0;
	m_totalProductsIncome = 0.0;
}

void BeautySaloon::setSaloonName(const std::string _saloonName)
{
	m_saloonName = _saloonName;
}

void BeautySaloon::changeCustomerMembership(Customer &_customer, const Membership::MemberType _memberType)
{
	for (Customer index : m_customers)
	{
		if (index.getName().compare(_customer.getName())==0)
		{
			index.setMemberType(_memberType);
			break;
		}
	}
}

void BeautySaloon::changeCustomerMembership(const std::string _name, const Membership::MemberType _memberType)
{
	for (Customer index : m_customers)
	{
		if (index.getName().compare(_name) == 0)
		{
			index.setMemberType(_memberType);
			break;
		}
	}
}

void BeautySaloon::addCustomer(Customer _customer)
{
	for (Customer index : m_customers)
		if (index.getName().compare(_customer.getName()) == 0)
			return;
	m_customers.push_back(_customer);
}

void BeautySaloon::addCustomer(const std::string _customerName)
{
	Customer newCustomer(_customerName);
	m_customers.push_back(newCustomer);
}

void BeautySaloon::addCustomer(const std::string _customerName, const Membership::MemberType _memberType)
{
	Customer newCustomer(_customerName, _memberType);
	m_customers.push_back(newCustomer);
}

void BeautySaloon::addVisit(const Customer _customer)
{
	m_visits.push_back(Visit(_customer, Date()));
}

void BeautySaloon::addVisit(const Customer _customer, const Date &_date)
{
	m_visits.push_back(Visit(_customer, _date));
}

void BeautySaloon::addVisit(Customer _customer, const Date &_date, const double _serviceExpense, const double _productExpense)
{
	m_visits.push_back(Visit(_customer, _date, _serviceExpense, _productExpense));
	m_totalProductsIncome += _productExpense - _productExpense * m_discountRates.getProductDiscount(_customer.getMemberType());
	m_totalServicesIncome += _serviceExpense - _serviceExpense * m_discountRates.getServiceDiscount(_customer.getMemberType());
}

void BeautySaloon::addVisit(Customer _customer, const double _serviceExpense, const double _productExpense)
{
	m_visits.push_back(Visit(_customer, Date(), _serviceExpense, _productExpense));
	m_totalProductsIncome += _productExpense - _productExpense * m_discountRates.getProductDiscount(_customer.getMemberType());
	m_totalServicesIncome += _serviceExpense - _serviceExpense * m_discountRates.getServiceDiscount(_customer.getMemberType());
}

void BeautySaloon::setProductDiscountRate(const Membership::MemberType _memberType, const double _discountRate)
{
	m_discountRates.setProductDiscount(_memberType, 100/_discountRate);		//if the discount rate is 20%, then we'll have to transform 20 into 0.2
}

void BeautySaloon::setServiceDiscountRate(const Membership::MemberType _memberType, const double _discountRate)
{
	m_discountRates.setServiceDiscount(_memberType, 100 / _discountRate);
}

void BeautySaloon::printCustomers()
{
	for (Customer customerIndex : m_customers)
	{
		std::cout << customerIndex.toString() << std::endl;
	}
}

double BeautySaloon::getServiceDiscountRate(const Membership::MemberType _memberType)
{
	return m_discountRates.getServiceDiscount(_memberType);
}

double BeautySaloon::getProductDiscountRate(const Membership::MemberType _memberType)
{
	return m_discountRates.getProductDiscount(_memberType);
}

double BeautySaloon::getTotalIncome()
{
	return m_totalProductsIncome + m_totalServicesIncome;
}

double BeautySaloon::getServicesIncome()
{
	return m_totalServicesIncome;
}

double BeautySaloon::getProductsIncome()
{
	return m_totalProductsIncome;
}


BeautySaloon::~BeautySaloon()
{
	m_visits.clear();
	m_customers.clear();
}
