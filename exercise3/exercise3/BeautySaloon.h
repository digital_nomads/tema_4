#pragma once
#include"Customer.h"
#include"DiscountRate.h"
#include"Visit.h"
#include<vector>

class BeautySaloon
{
	std::string m_saloonName;
	std::vector<Visit> m_visits;
	std::vector<Customer> m_customers;
	DiscountRate m_discountRates;;
	double m_totalServicesIncome;
	double m_totalProductsIncome;
public:
	BeautySaloon();
	BeautySaloon(const std::string);
	BeautySaloon(const std::string, const DiscountRate);
	void setSaloonName(const std::string);
	void changeCustomerMembership(Customer&, const Membership::MemberType);
	void changeCustomerMembership(const std::string _name, const Membership::MemberType);
	void addCustomer(Customer);
	void addCustomer(const std::string);
	void addCustomer(const std::string, const Membership::MemberType);
	void addVisit(const Customer);					//visit without any spendings
	void addVisit(const Customer, const Date&);		//		--||--
	void addVisit(Customer, const Date&, const double, const double);
	void addVisit(Customer, const double, const double);
	void setProductDiscountRate(const Membership::MemberType,const double);
	void setServiceDiscountRate(const Membership::MemberType,const double);
	void printCustomers();
	double getServiceDiscountRate(const Membership::MemberType);
	double getProductDiscountRate(const Membership::MemberType);
	double getTotalIncome();
	double getServicesIncome();
	double getProductsIncome();
	~BeautySaloon();
};

