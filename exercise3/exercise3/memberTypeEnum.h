#pragma once

namespace Membership
{
	enum MemberType
	{
		Premium,
		Gold,
		Silver
	};
}