#include "Visit.h"



Visit::~Visit()
{
}

Visit::Visit(const Customer& _customer, const  Date _date, const  double _serviceExpense, const  double _productExpense)
{
	m_customer = _customer;
	m_date = _date;
	m_serviceExpense = _serviceExpense;
	m_productExpense = _productExpense;
}

Visit::Visit(const Customer& _customer, const  Date _date)
{
	m_customer = _customer;
	m_date = _date;
	m_serviceExpense = 0.0;
	m_productExpense = 0.0;
}

std::string Visit::getCustomerName()
{
	return m_customer.getName();
}

double Visit::getServiceExpense()
{
	return m_serviceExpense;
}

Customer Visit::getCustomer()
{
	return m_customer;
}

void Visit::setServiceExpense(const double _expense)
{
	m_serviceExpense = _expense;
}

double Visit::getProductExpense()
{
	return m_productExpense;
}

void Visit::setProductExpense(const double _expense)
{
	m_productExpense = _expense;
}

double Visit::getTotalExpense()
{
	return m_productExpense + m_serviceExpense;
}

std::string Visit::toString()
{
	return m_customer.getName() + " " + m_date.toString() + " - Total Expense: " + std::to_string(getTotalExpense());
}
