#pragma once
#include"Customer.h"
#include"Date.h"
class Visit
{
private:
	Customer m_customer;
	Date m_date;
	double m_serviceExpense;
	double m_productExpense;
public:
	Visit(const Customer&, const Date, const  double, const  double);
	Visit(const Customer&, const Date);
	std::string getCustomerName();
	double getServiceExpense();
	Customer getCustomer();
	void setServiceExpense(const double);
	double getProductExpense();
	void setProductExpense(const double);
	double getTotalExpense();
	std::string toString();
	~Visit();
};

