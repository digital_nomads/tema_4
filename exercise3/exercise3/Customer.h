#pragma once
#include<string>
#include"memberTypeEnum.h"
class Customer
{
private:
	std::string m_name;
	Membership::MemberType m_memberType;
	bool m_member;
public:
	Customer();
	Customer(const std::string, const  Membership::MemberType);
	Customer(const std::string);
	void setMemberType(const Membership::MemberType);
	bool isMember();
	Membership::MemberType getMemberType();
	std::string getName();
	std::string toString();
	~Customer();
};

