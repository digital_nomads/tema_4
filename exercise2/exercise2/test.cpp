#include"CreditCard.h"
using namespace std;
void main()
{
	try
	{
		CreditCard myCard("Andrei", "27051997");
		//cin >> myCard;
		myCard.charge("ApeHanger HandleBars 20\"", Money(50, 0));
		myCard.charge("Gasoline", 20, 50);
		myCard.charge("Gasoline", Money(30, 0));
		myCard.charge("Gasoline", 20, 50);
		myCard.charge("Gasoline", 30, 0);
		myCard.printTransactions();

		cout << endl << myCard;
		cin.get();
		cin.get();
	}
	catch(const std::exception &e)
	{
		cout<<e.what();
		cin.get();
	}
}