#include "CreditCard.h"
#include <exception>


double CreditCard::makeDouble(int _euros, int _centimes)
{
	if (_euros < 0 || _centimes < 0)
		throw std::exception("Moneys can't have negative values!");
	while (_centimes > 100)
	{
		++_euros;
		_centimes -= 100;
	}
	return _euros + (double)_centimes / 100;
}
CreditCard::CreditCard()
{
	
}

CreditCard::CreditCard(std::string _ownerName, std::string _cardNumber)
{
	if (_ownerName.empty() || _cardNumber.empty())
		throw std::exception("Invalid card data!");
	m_ownerName = _ownerName;
	m_cardNumber = _cardNumber;
}

void CreditCard::printTransactions()
{
	for (std::map<std::string, double>::const_iterator it = m_transactions.begin(); it != m_transactions.end(); ++it)
		std::cout << it->first << ": " << it->second << std::endl;
}

void CreditCard::charge(const std::string _itemName,const int _euros,const int _centimes)
{
	if (_euros < 0 || _centimes < 0)
		throw std::exception("Items cannot have negative values!");
	m_transactions.insert(std::make_pair(_itemName, makeDouble(_euros, _centimes)));
}


void CreditCard::charge(const std::string _itemName, Money _money)
{
	m_transactions.insert(std::make_pair(_itemName, makeDouble(_money.getEuros(), _money.getCentimes())));
}

void CreditCard::setOwnerName(const std::string _ownerName)
{
	m_ownerName = _ownerName;
}

void CreditCard::setCardNumber(const std::string _cardNumber)
{
	m_cardNumber = _cardNumber;
}

std::string CreditCard::getOwnerName()
{
	return m_ownerName;
}

std::string CreditCard::getCardNumber()
{
	return m_cardNumber;
}

CreditCard::~CreditCard()
{
	m_transactions.clear();
	m_ownerName.clear();
	m_cardNumber.clear();
}

std::istream & operator>>(std::istream & inputStream, CreditCard & _creditCard)
{
	std::string ownerName, cardNumber;
	char currentChar;
	inputStream.get(currentChar);
	while (currentChar != '\n')
	{
		ownerName += currentChar;
		inputStream.get(currentChar);
	}
	inputStream >> cardNumber;
	_creditCard.setOwnerName(ownerName);
	_creditCard.setCardNumber(cardNumber);

	return inputStream;
}

std::ostream & operator<<(std::ostream & outputStream, CreditCard _creditCard)
{
	outputStream << _creditCard.getOwnerName() << " - " << _creditCard.getCardNumber();
	return outputStream;
}
