#pragma once
#include<iostream>
class Money
{
private:
	int m_euros;
	int m_centimes;
public:
	Money();
	void print();
	Money(const int _euro,const int _centimes);
	void setEuros(const int _euros);
	void setCentimes(const int _centimes);
	int getEuros();
	int getCentimes();
	~Money();

	friend std::istream &operator>>(std::istream &inputStream, Money &_money);
	friend std::ostream &operator<<(std::ostream &outStream, Money _money);

	Money operator+(Money _money);
	Money operator-(Money _money);
	Money operator*(Money _money);
	Money operator/(Money _money);

	bool operator==(Money _money);
	bool operator!=(Money _money);
	bool operator<(Money _money);
	bool operator<=(Money _money);
	bool operator>(Money _money);
	bool operator>=(Money _money);
};

