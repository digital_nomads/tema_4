#include "Money.h"
#include <exception>


Money::Money()
{
	m_euros = 0;
	m_centimes = 0;
}

void Money::print()
{
	if (m_centimes < 10)
		std::cout << m_euros << ",0" << m_centimes << " Euros";
	else
		std::cout << m_euros << ',' << m_centimes << " Euros";
}

Money::Money(const int _euros,const int _centimes)
{
	if (_euros < 0 || _centimes < 0)
		throw std::exception("Moneys can't have negative values!");
	m_euros = _euros;
	m_centimes = _centimes;
	while(m_centimes >= 100)
	{
		m_euros++;
		m_centimes -= 100;
	} 
}

void Money::setEuros(const int _euros)
{
	if (_euros < 0)
		throw std::exception("Can't assign negative values!");
	m_euros = _euros;
}


void Money::setCentimes(const int _centimes)
{
	if (_centimes < 0)
		throw std::exception("Can't assign negative values!");
	m_centimes = _centimes;
	while (m_centimes >= 100)
	{
		m_euros++;
		m_centimes -= 100;
	}
}

int Money::getEuros()
{
	return m_euros;
}

int Money::getCentimes()
{
	return m_centimes;
}

Money::~Money()
{
}

Money Money::operator+(Money _money)
{
	return Money(m_euros + _money.getEuros(), m_centimes + _money.getCentimes());
}

Money Money::operator-(Money _money)
{
	Money returnedMoney = Money(m_euros - _money.getEuros(), m_centimes - _money.getCentimes());
	return returnedMoney;
}

Money Money::operator*(Money _money)
{
	double _moneyFloatValue = _money.getEuros() + (double)_money.getCentimes() / 100;
	double m_moneyFloatValue = m_euros + (double)m_centimes / 100;
	double result = m_moneyFloatValue * _moneyFloatValue;
	return Money((int)result, (int)(result * 100) % 100);
}

Money Money::operator/(Money _money)
{
	double _moneyFloatValue = _money.getEuros() + (double)_money.getCentimes()/100;
	if (_moneyFloatValue == 0.0)
		throw std::exception("Cannot divide by 0!");
	double m_moneyFloatValue = m_euros + (double)m_centimes/100;
	double result = m_moneyFloatValue / _moneyFloatValue;
	return Money((int)result, (int)(result*100)%100);
}

bool Money::operator==(Money _money)
{
	return (m_euros == _money.getEuros() && m_centimes == _money.getCentimes());
}

bool Money::operator!=(Money _money)
{
	return !(*this == _money);
}

bool Money::operator<(Money _money)
{
	if (m_euros == _money.getEuros())
	{
		if (m_centimes < _money.getCentimes())
			return true;
	}
	if (m_euros < _money.getEuros())
		return true;
	return false;
}

bool Money::operator<=(Money _money)
{
	if (*this < _money || *this == _money)
		return true;
	return false;
}

bool Money::operator>(Money _money)
{
	return !(*this < _money || *this==_money);
}

bool Money::operator>=(Money _money)
{
	return (*this > _money || *this == _money);
}

std::istream & operator>>(std::istream & inputStream, Money & _money)
{
	int value;
	inputStream >> value;
	_money.setEuros(value);
	inputStream >> value;
	_money.setCentimes(value);
	return inputStream;
}

std::ostream & operator<<(std::ostream & outStream, Money _money)
{
	outStream << _money.getEuros() << ',';
	if (_money.getCentimes() < 10)
		outStream << '0';
	outStream << _money.getCentimes();
	return outStream;
}
