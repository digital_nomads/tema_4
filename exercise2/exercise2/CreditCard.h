#pragma once
#include<iostream>
#include<string>
#include<map>
#include "Money.h"

class CreditCard
{
private:
	std::string m_ownerName;
	std::string m_cardNumber;
	std::multimap < std::string, double> m_transactions;
	double makeDouble(int _euros, int _centimes);
public:
	CreditCard();
	CreditCard(const std::string _ownerName, const std::string _cardNumber);
	void printTransactions();
	void charge(const std::string _itemName,const int _euros, const int _centimes);
	void charge(const std::string _itemName, Money _money);
	void setOwnerName(const std::string _ownerName);
	void setCardNumber(const std::string _cardNumber);
	std::string getOwnerName();
	std::string getCardNumber();
	~CreditCard();

	friend std::istream &operator>>(std::istream &inputStream, CreditCard &_creditCard);
	friend std::ostream &operator<<(std::ostream &outputStream, CreditCard _creditCard);
};

